# about

Information on the content and structure of this website as well as news

This repository is an optional module for the "lerntools". 

For installation and configuration, please see the documentation in https://codeberg.org/lerntools/base

------------------

In diesem opionalen Modul können sie Texte hinterlegen, wie z.B. Informationen über Inhalt und Struktur der Webseite oder Neuigkeiten.

Für genauere Informationen wie sie das Modul anschauen, beachten sie bitte die Dokumentation in /base

*So fügen Sie Texte hinzu:*

Im Ordner "about" den Ordner /text/ anlegen und einen Unterordner für die verwendete Sprache (z.B. de und en). In diesem eine index.md und eventuelle andere .md Dateien, auf die verwiesen wird.