
import Icon from './img/about.svg';
const meta = require('./locales/Index.json');
let infos = {'title': {}, 'subtitle': {}}
for (const lang in meta) {
	if ('title' in meta[lang]) {
		infos['title'][lang] = meta[lang]['title'];
	} else {
		infos['title'][lang] = "";
	}
	if ('subtitle' in meta[lang]) {
		infos['subtitle'][lang] = meta[lang]['subtitle'];
	} else {
		infos['subtitle'][lang] = "";
	}
}

export default {
	id: "about",
	meta: {
		title: infos['title'],
		text: infos['subtitle'],
		to: 	"about-index",
		icon: 	Icon,
		index:	true,
	},
	routes: [
		{	path: '/about-index', name:'about-index', redirect: { 
				name: 'main-text-tpl', params: {module: 'about', file: 'index'} 
		}},
	]
}
